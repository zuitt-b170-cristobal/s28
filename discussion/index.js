//JS Synchronous vs JS Asynchronous
//Syngchronous programming - only 1 statement/line of code is being processes at a time; Used by JS by default

//error checking proves the synchronous programming of JS since after detecting the error, the next lines of codes will not be executed even if they have no errors
console.log("Hello World");
//console.log("Hello again");
console.log("Goodbye");

console.log("Hello World");
/*for (let i = 0; i<=1500; i++) {
	console.log(i);
//when certain statements take a lot of time to process, this slows down the running/executing of codes; ex. when loops are used on a large amt of info or when fetching data from database; when an action takes time to be executed, result is CODE BLOCKING
};*/
console.log("Hello Again");
//code blocking - delaying of more efficient code compared to ones currently executed

/*ASYNCHRONOUS PORGRAMMING


*/



//FETCH FUNCTION/Fetch API
	//allows to request asynchronously for a  resource (data)
	//promise - an object that represents eventual completion or failure of an asynchronous function & its resulting; means it is meant to be caught by another code that follows the fetch URL 
//SYNTAX
/*
	fetch('URL')
*/
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));


/*SYNTAX
	fetch(url).then((parameter) => statement).then((parameter) => statement)


*/


//retrieves all posts following the RET  API method (read/GET)
//by using the .then method, we can now check the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")

//since fetch method returns a "promise", the "then" method will catch the promise & make it the "response" object

//also, the then method captures the "resposne" object & returns another promise w/c will eventually be rejected/resolved

.then(response => console.log(response));
//response(dev defined) came from fetch(URL) that serves as parameter; in object format so console cannot load data

//this will be logged first, before the status of the promise(result of the .then())
console.log("Hello Again");

fetch("https://jsonplaceholder.typicode.com/posts")
//this .response is in object, being passed as a parameter & convert to json as a response to another .then() to log json
.then((response)=> response.json())

//using the methods multiple times would create a promise chain; needs another .then() promise to log json in the console otherwise will be undefined;
.then((json) => console.log(json));
/*
1.the use of "json()" is to convert the response object into json format to be used by the app
2.since we cannot directly print json format of response in the second .then method, we need another .then method to catch the promise and print the "response.json()" w/c is being represented by the "json" parameter
3.using the methods multiple times would create a promise chain
*/

//ASYNC-AWAIT
	//another approach that can be used to achieve JS asynchronous
	//used in functions to indicate w/c portions of code shouled be waited
	//cosed outside the functions will be executed under JS asynchronous
async function fetchData() {
	//waits for the fetch method to be done before storing value of the response in the "result"
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	console.log(result);
	console.log(typeof result);
	console.log(result.body);
	//cannot log result bec wrong format, needs converting to json; converts data from the "results" variable into json & stores it in "json" variable
	let json = await result.json();
	console.log(json);
}
fetchData();
console.log("Hello Again");

//MINIACTIVITY - using .then method

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response)=> response.json())
.then((json) => console.log(json[0]));

/*ANOTHER WAY - GET method
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response)=> response.json())
.then((json) => console.log(json));
*/


//CREATE/UPDATE A RESOURCE
/*SYNTAX
	fetch ("url", {options}, details of the request body)
	.then(response => {statement})
	.then((json) => {statement})
*/
//using POST method to create object (post/:id, POST)
fetch("https://jsonplaceholder.typicode.com/posts",{
	method: "POST",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "New Post",
		body: "Hello World"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//MINIACTIVITY -PUT method (changes the whole object)

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		title: "corrected post",
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//MINIACTIVITY - PATCH method (replaces only the specified fields)

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PATCH",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "Update Post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "DELETE"
})

/*
Filtering posts
	data/result coming from fetch method can be filtered by sending a key-value pair along w/ its URL

	the info is sent via the URL can be done by adding question mark symbol(?)

	SYNTAX
	"url?parameterName=value"
	for multiple parameters:
	"url?paramA=valueA&paramB=valueB" 
*/

//filtering using single parameter
//fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
//filtering using multiple parameter
//fetch("https://jsonplaceholder.typicode.com/posts?userId=1&id=3")
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response)=> response.json())
.then((json) => console.log(json));

//MINIACTIVITY
//Retrieving nested/related comments to posts
//GET Method url/posts/:id/comments

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response)=> response.json())
.then((json) => console.log(json));