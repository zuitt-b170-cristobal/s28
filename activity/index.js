const url = `https://jsonplaceholder.typicode.com/todos`

//3
fetch(url)
.then((response)=> response.json())
.then((json) => console.log(json));

//4 Using data retrieved, create an array using the map method to return just titles of every item & print result in console
async function fetchData() {
	let result = await fetch(url)
	let json = await result.json();
	console.log(json.map(json => json.title));
}
fetchData();



//5
fetch(url)
.then((response)=> response.json())
.then((json) => console.log(json[0]));

//6 using data retrieved, print a message in console that provides title & status of todo list item
async function data() {
	let result = await fetch("https://jsonplaceholder.typicode.com/todos/1")
	let json = await result.json();
	console.log(`the item ${json.title} on the list has a status of ${json.completed}`)
}
data();


//7
fetch(url,{
	method: "POST",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "New Post",
		completed: "false"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//8
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		title: "corrected post",
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//9
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		Title: "change data structure post",
		Description: "lorem ipsum text",
		Status: "Completed",
		dateCompleted: "2022-03",
		userId: 2
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//10
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "Update Post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//11
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		Status: "Completed",
		dateCompleted: "2022-03"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//12
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
})